import React from "react";
import { Link } from 'react-router-dom';

function Footer() {
    return (
      <section id="footer" class="footer py-7">
        <div class="container">
            <div class="footer-top">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="logo"><Link to="#"><span>Le</span> <span>Restaurant</span></Link></div>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui corrupti dignissimos 
                            at quibusdam iure nihil pariatur incidunt enim dolores aliquam, exercitationem, illum 
                            minimLink repellat rerum nobis reprehenderit magni? Facilis, quibusdam.
                        </p>
                        <div class="socials">
                            <ul>
                                <li><Link to="#"><i class="fab fa-twitter sh"></i></Link></li>
                                <li><Link to="#"><i class="fab fa-pinterest sh"></i></Link></li>
                                <li><Link to="#"><i class="fab fa-facebook-f sh"></i></Link></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="menu-footer">
                            <h3>Lire Plus</h3>
                            <ul>
                                <li><Link to="#">Blog</Link></li>
                                <li><Link to="#">Accueil</Link></li>
                                <li><Link to="#">Link Propos</Link></li>
                                <li><Link to="#">Nous Joindre</Link></li>
                                <li><Link to="#">Vos Commandes</Link></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="menu-footer">
                            <h3>Ressources</h3>
                            <ul>
                                <li><Link to="#">CGU</Link></li>
                                <li><Link to="#">Visitez-nous</Link></li>
                                <li><Link to="#">Centre d'Aide</Link></li>
                                <li><Link to="#">Politique Confidentialité</Link></li>
                                <li><Link to="#">Condition d'Utilisation</Link></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="menu-footer">
                            <h3>Contacts Rapide</h3>
                            <ul>
                                <li><Link to="#"><i class="fas fa-phone mr-2"></i> +123456789</Link></li>
                                <li><Link to="#"><i class="fas fa-envelope mr-2"></i> info@lerestaurant.com</Link></li>
                                <li><Link to="#"><i class="fas fa-map-marker-alt mr-2"></i>732/21 Second Street, Manchester, King Street</Link></li>
                                <li><Link to="#">Politique Confidentialité</Link></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </section>
      
    );
}


export default Footer;