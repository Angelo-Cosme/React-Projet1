import React from 'react';
import Slider from '../inc/slide';
import Location from '../inc/location';
import BreakFast from '../inc/breakfast';
import Reservation from '../inc/reservation';
import Lunch from '../inc/lunch';
import Dinner from '../inc/dinner'


class Home extends React.Component {
  render() {
    return(
      <div>
        <Slider/>
        <Location/>
        <BreakFast/>
        <Reservation/>
        <Lunch/>
        <Dinner/>
      </div>
    );
  }
}

export default Home;
