import React from "react";
import Slider1 from '../images/monrestaurant.jpg';


function Slider () {
    return(
     <div id="slide" style={{ backgroundImage: `url(${Slider1})` }}>
       <div className="container">
        <div className="row">
          <div class="col-lg-9 col-xl-7 mx-auto">
            <div className="content">
              <span>MouMou Rest !</span>
              <h1>Lorem ipsum dolor sit amet consec</h1>
            </div>
          </div>
        </div>
       </div>
     </div>
    );
}

export default Slider;