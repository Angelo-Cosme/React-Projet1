import React from "react";
import { Link } from 'react-router-dom';
import mapimg from '../images/map-image-1.jpg';

function Location() {
    return (
      <section id="location" class="location py-7">
        <div class="container">
          <div class="row">
            <div class="col-lg-10 col-xl-8 text-center mx-auto">
              <h2> JE DÉCOUVRE LE GRAND RESTO, JE N'AI JAMAIS MAIS AUSSI BIEN !</h2>
              <span>Lorem ipsum dolor sit amet dolor sit amet.</span>
              <p class="para">John Deo</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8">
              <img src={mapimg} class="img-fluid" alt="map !" />
            </div>
            <div class="col-lg-4">
              <div class="own text-center">
                <span>Discover</span>
                <h4>Notre Histoire</h4>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia, consectetur odit! Et, 
                  sint dicta! Quae vitae soluta impedit eius voluptatum. Esse illum officiis reiciendis 
                  delectus blanditiis vero totam placeat ad.</p>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia, consectetur odit! Et, 
                    sint dicta! Quae vitae soluta impedit eius voluptatum. Esse illum officiis reiciendis 
                    delectus blanditiis vero totam placeat ad.
                  </p>
                  <Link to="/">Lire plus</Link>
                </div>
              </div>
            </div>
        </div>
      </section>
    );
}


export default Location;