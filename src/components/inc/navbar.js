import React from "react";
import { Link } from 'react-router-dom';

function Navbar() {
    return (
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container">
					<Link class="navbar-brand" to="/"><span>Le</span> <span>Restaurant</span></Link>
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#Restaurant" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse justify-content-end" id="Restaurant">
						<div class="navbar-nav">
							<Link to="/">Accueil</Link>
							<Link to="/">Nos Menus</Link>
							<Link to="/About">A Propos</Link>
							<Link to="/" className="btn-res">Réservation</Link>
						</div>
					</div>
				</div>
			</nav>
    );
}


export default Navbar;