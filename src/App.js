import './App.css';
import './theme.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Home from './components/pages/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import Navbar from './components/inc/navbar';
import Footer from './components/inc/footer';
import FooterBottom from './components/inc/footerbottom';


function App() {
  return (
    <>
      <Router>
      <Navbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/About" element={<About />} />
          <Route path="/Contact" element={<Contact />} />
        </Routes>
      <Footer/>
      <FooterBottom/>
      </Router>
    </>
  );
}


export default App;
