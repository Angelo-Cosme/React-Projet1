import React from "react";
import { Link } from 'react-router-dom';
import Cards from '../images/cards.png';

function FooterBottom() {
    return (
      <section class="footer-bottom py-3">
				<div class="content">
					<div class="container">
						<div class="row">
							<div class="col-lg-8">
								<p>© Copyright Le Restaurant Theme Propre - Développé Par <Link to="">Angelo VODOUMBO</Link></p>
							</div>
							<div class="col-lg-4">
								<img src={Cards} alt="" />
							</div>
						</div>
					</div>
				</div>
			</section>
      
    );
}


export default FooterBottom;