import React from "react";
import { Link } from 'react-router-dom';
import ReservationImg from '../images/reservation.jpg';

function Reservation() {
    return (
      <section id="reservation" class="reservation py-11" style={{ backgroundImage: `url(${ReservationImg})` }}>
        <div class="container">
          <div class="row">
            <div class="col-lg-10 col-xl-8 text-center mx-auto">
              <Link to="#" class="btn">Obtenez une réservation</Link>
              <p>Ou Appelez le +41234567043</p>
            </div>
          </div>
        </div>
      </section>
    );
}


export default Reservation;