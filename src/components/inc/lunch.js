import React from "react";
import { Link } from 'react-router-dom';
import Lunch from '../images/lunchmenu.jpg';

function BreakFast() {
    return (
      <section id="menu-lunch" class="menu-lunch py-11">
        <div class="container">
          <div class="row">
            <div class="col-lg-10 col-lg-8 text-center mx-auto">
              <span>Lorem ipsum dolor sit amet consectetur adipisicing.</span>
              <h2>Lunch Menu</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-7">
              <div class="row">
                <div class="col-md-6">
                  <div class="list-items">
                    <div class="list-item">
                      <div class="row">
                        <div class="col-md-10">
                          <h5>Slow roasted white</h5>
                          <p>Duis aute irure dolor in reprehenderit i esse</p>
                        </div>
                        <div class="col-md-2"><span>9.5$</span></div>
                      </div>
                    </div>
                    <div class="list-item">
                      <div class="row">
                        <div class="col-md-10">
                          <h5>Slow roasted white</h5>
                          <p>Duis aute irure dolor in reprehenderit i esse</p>
                        </div>
                        <div class="col-md-2"><span>9.5$</span></div>
                      </div>
                    </div>
                    <div class="list-item">
                      <div class="row">
                        <div class="col-md-10">
                          <h5>Slow roasted white</h5>
                          <p>Duis aute irure dolor in reprehenderit i esse</p>
                        </div>
                          <div class="col-md-2"><span>9.5$</span></div>
                      </div>
                    </div>
                    <div class="list-item">
                      <div class="row">
                        <div class="col-md-10">
                          <h5>Slow roasted white</h5>
                          <p>Duis aute irure dolor in reprehenderit i esse</p>
                        </div>
                        <div class="col-md-2"><span>9.5$</span></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="list-items">
                    <div class="list-item">
                      <div class="row">
                        <div class="col-md-10">
                          <h5>Slow roasted white</h5>
                          <p>Duis aute irure dolor in reprehenderit i esse</p>
                        </div>
                        <div class="col-md-2"><span>9.5$</span></div>
                      </div>
                    </div>
                    <div class="list-item">
                      <div class="row">
                        <div class="col-md-10">
                          <h5>Slow roasted white</h5>
                          <p>Duis aute irure dolor in reprehenderit i esse</p>
                        </div>
                        <div class="col-md-2"><span>9.5$</span></div>
                      </div>
                    </div>
                    <div class="list-item">
                      <div class="row">
                        <div class="col-md-10">
                          <h5>Slow roasted white</h5>
                          <p>Duis aute irure dolor in reprehenderit i esse</p>
                        </div>
                        <div class="col-md-2"><span>9.5$</span></div>
                      </div>
                    </div>
                    <div class="list-item">
                      <div class="row">
                        <div class="col-md-10">
                          <h5>Slow roasted white</h5>
                          <p>Duis aute irure dolor in reprehenderit i esse</p>
                        </div>
                        <div class="col-md-2"><span>9.5$</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <Link to="#" class="btn">Commander Votre Menu</Link>
            </div>
						<div class="col-lg-5">
              <img src={Lunch} class="img-fluid" alt=""/>
            </div>
          </div>
        </div>
      </section>
    );
}


export default BreakFast;